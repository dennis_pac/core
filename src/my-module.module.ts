import { AuthService } from './providers/auth.service';
import { HttpService } from './providers/http.service';
import { Observable } from 'rxjs';
import { NgModule, ModuleWithProviders } from '@angular/core';

@NgModule({
  declarations: [
    // declare all components that your module uses
  ],
  exports: [
    // export the component(s) that you want others to be able to use
  ]
})
export class MyModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MyModule,
      providers: [ HttpService, AuthService ]
    };
  }
}

