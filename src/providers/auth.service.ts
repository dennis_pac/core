import { HttpService } from './http.service';
import { Injectable, OnDestroy } from '@angular/core';
import { Headers, Http, RequestOptionsArgs, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';
import { ResponseBean } from '../models/data-holder.model';

export class AuthBean {
    email: string;
    token: string;

    constructor(email: string, token: string) {
        this.email = email;
        this.token = token;
    }
}

@Injectable()
export class AuthService implements OnDestroy {
    
    private authBean: AuthBean;

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    constructor(private http: Http,
                private myHttpService:HttpService) {
    }

    ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
        this.authBean = null;
    }

    login(email: string, password: string): Promise<boolean> {
        return new Promise<boolean>( (resolve: any, reject: any) => {

            const data: URLSearchParams = new URLSearchParams();
            data.append('email', email);
            data.append('password', password);

            this.myHttpService.post('login', true, data)
            .subscribe(
                (dataHolder: ResponseBean) => {
                    console.log(dataHolder);
                    console.log(dataHolder.error);
                    if (dataHolder.error == null || dataHolder.error.length == 0 || this.isObjectEmpty(dataHolder.error)) {
                        resolve(true);
                        this.authBean = <AuthBean> dataHolder.success;
                        localStorage.setItem('token', this.authBean.token);
                        // console.log('User: ' + this.authBean.username + ', Roles: ' + this.authBean.roles);
                        // this.myCommunicationLink.broadcast('login-done');
                    } else {
                        resolve(false);
                        // this.myCommunicationLink.broadcast('login-error');
                    }
                },
                (error: any) => {
                    resolve(false);
                    // this.myCommunicationLink.broadcast('login-error');
                }
            );
        });
    }

    regist(email: string, username: string, password: string): Promise<boolean> {
        return new Promise<boolean>( (resolve: any, reject: any) => {
            const headers: Headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');

            const options: RequestOptionsArgs = {
                withCredentials: true,
                headers: headers
            };

            const data: URLSearchParams = new URLSearchParams();
            data.append('email', email);
            data.append('password', password);

            this.myHttpService.post('regist', true, data)
            .subscribe(
                (dataHolder: ResponseBean) => {
                    console.log(dataHolder);
                    console.log(dataHolder.error);
                    if (dataHolder.error == null || dataHolder.error.length == 0 || this.isObjectEmpty(dataHolder.error)) {
                        resolve(true);
                        this.authBean = <AuthBean> dataHolder.success;
                        localStorage.setItem('token', this.authBean.token);
                        // console.log('User: ' + this.authBean.username + ', Roles: ' + this.authBean.roles);
                        // this.myCommunicationLink.broadcast('login-done');
                    } else {
                        resolve(false);
                        // this.myCommunicationLink.broadcast('login-error');
                    }
                },
                (error: any) => {
                    resolve(false);
                    // this.myCommunicationLink.broadcast('login-error');
                }
            );
        });
    }

    logout(): Promise<boolean> {
        return new Promise<boolean>( (resolve: any, reject: any) => {
            resolve(true);
            this.authBean = null;
            localStorage.removeItem('token');
        });
    }

    public isLoggedIn(): boolean {
        return !!localStorage.getItem('token');
    }

    public getAuthBean(): AuthBean {
        return this.authBean;
    }
      
    public getToken() : string {
        return localStorage.getItem('token');
    }
    
    public isObjectEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
}
