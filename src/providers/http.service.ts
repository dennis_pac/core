import { AuthService } from './auth.service';
import { Http, Request, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseBean } from '../models/data-holder.model';

@Injectable()
export class HttpService {

    constructor(private myHttp: Http) {
    }

    /**
     * (usually) for cache invalidation of SELECTs
     */
    public head(target: string, withCredentials: boolean, aErrHandler?: (err: any) => any): Observable<{} | ResponseBean> {
        const headers = this.createHeaders();
        const options: RequestOptionsArgs = this.createRequestOptionsArgs(withCredentials, headers);
        const url = this.getURL(target);
        return this.myHttp.head(url, options)
            .map(this.toResponseBean)
            .catch((err: any) => this.handleErr(err, aErrHandler));
    }

    /**
     * (usually) for SELECTs
     */
    public get(target: string, withCredentials: boolean, aErrHandler?: (err: any) => any, requestOptions?: RequestOptionsArgs): Observable<{} | ResponseBean> {
        const headers = this.createHeaders();
        const options: RequestOptionsArgs = this.createRequestOptionsArgs(withCredentials, headers);
        const url = this.getURL(target);
        return this.myHttp.get(url, options)
            .map(this.toResponseBean)
            .catch((err: any) => this.handleErr(err, aErrHandler));
    }

    /**
     * (usually) for DELETEs
     */
    public delete(target: string, withCredentials: boolean, aErrHandler?: (err: any) => any): Observable<{} | ResponseBean> {
        const headers = this.createHeaders();
        const options: RequestOptionsArgs = this.createRequestOptionsArgs(withCredentials, headers);
        const url = this.getURL(target);
        return this.myHttp.delete(url, options)
            .map(this.toResponseBean)
            .catch((err: any) => this.handleErr(err, aErrHandler));
    }

    /**
     * (usually) for INSERTs
     */
    public post(target: string, withCredentials: boolean, params: URLSearchParams, aErrHandler?: (err: any) => any): Observable<{} | ResponseBean> {
        if(params == null) {
            params = new URLSearchParams();
        }
        params.append('token', this.getToken());
        const body:any = params.toString();
        const headers = this.createHeaders();
        const options: RequestOptionsArgs = this.createRequestOptionsArgs(withCredentials, headers);
        const url = this.getURL(target);
        return this.myHttp.post(url, body, options)
            .map(this.toResponseBean)
            .catch((err: any) => this.handleErr(err, aErrHandler));
    }

    /**
     * (usually) for partial UPDATESs (rarely implemented!)
     */
    public patch(target: string, withCredentials: boolean, body: any, aErrHandler?: (err: any) => any): Observable<{} | ResponseBean> {
        const headers = this.createHeaders();
        const options: RequestOptionsArgs = this.createRequestOptionsArgs(withCredentials, headers);
        const url = this.getURL(target);
        return this.myHttp.patch(url, body, options)
            .map(this.toResponseBean)
            .catch((err: any) => this.handleErr(err, aErrHandler));
    }

    /**
     * (usually) for UPDATEs
     */
    public put(target: string, withCredentials: boolean, body: any, aErrHandler?: (err: any) => any): Observable<{} | ResponseBean> {
        const headers = this.createHeaders();
        const options: RequestOptionsArgs = this.createRequestOptionsArgs(withCredentials, headers);
        const url = this.getURL(target);
        return this.myHttp.put(url, body, options)
            .map(this.toResponseBean)
            .catch((err: any) => this.handleErr(err, aErrHandler));
    }

    private toResponseBean: ((response: Response) => (ResponseBean)) = function (response: Response): ResponseBean {
        return <ResponseBean>response.json();
    };

    protected getURL(target: string): string {
        // return this.mySiteService.getConfigKey('rest') + target;
        return 'http://localhost:9000/' + target;
    }

    protected createRequestOptionsArgs(withCredentials: boolean, headers?: Headers): RequestOptionsArgs {
        const options: RequestOptionsArgs = {
            // withCredentials: false,
        };
        if (typeof headers !== 'undefined' && headers !== null) {
            options.headers = headers;
        }
        return options;
    }

    protected createHeaders(): Headers {
        const headers: Headers = new Headers();
        /*headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
        headers.append('Access-Control-Allow-Credentials', 'true');
        headers.append('Access-Control-Allow-Headers','Authentication , Authorization , X-CSRF-Token , Access-Control-Allow-Credentials , Access-Control-Allow-Methods , Access-Control-Allow-Origin , Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        */
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
        return headers;
    }

    protected handleErr(err: HttpErrorResponse, aErrHandler?: ((err: any) => (any))): any {
        console.log('error');
        console.log(err);
        if (typeof aErrHandler === 'undefined' || aErrHandler === null) {
            console.error('-> default errorhandler');

            if (err.status === 403) {
                console.error('Request returned with 403 - unauthorized');
                // this.myCommunicationLink.broadcast('auth-error');
            } else if (typeof err[ '_body' ] !== 'undefined' && err[ '_body' ] != null) {
                try {
                    const errobj = JSON.parse(err[ '_body' ]);
                    // TODO ERRORR ALS JSON! ' + err.error.json());
                    /*this.myMessageService.error(
                        this.myTranslateService.instant('sla.ui.message.error.label'),
                        errobj.errordesc);
                        */
                } catch (ex) {
                    /*this.myMessageService.error(
                        this.myTranslateService.instant('sla.ui.message.error.label'),
                        `http statusCode [${err.status}]`);*/
                }
            } else {
                /*this.myMessageService.error(
                    this.myTranslateService.instant('sla.ui.message.error.label'),
                    `http statusCode [${err.status}]`);*/
            }
        } else {
            console.error('-> delegating to custom errorhandler');
            aErrHandler(err);
        }
        return Observable.throw(err);
    }
     
    public getToken() : string {
        return localStorage.getItem('token');
    }
}
